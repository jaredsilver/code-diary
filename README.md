# README

### 🚀 Installation/viewing instructions
This is a standard rails app. Clone, bundle install, run `rails s`, and view at port 3000.

### ⚠️ Biggest issue you ran into
Time. I didn't get around to adding tests or covering some basic edge cases (e.g. there's no non-empty/non-null constraint at the controller or database level.)

### 🧠 What you learned
There weren't many learnings associated with this project given that I used the trusty old Rails stack. I haven't worked in a new Rails 5+ app though, so some of the best practices around e.g. form helpers have changed, and I was surprised by this.

### 💡 What you would have done differently
I wouldn't have done anything differently, but with more time here's what I would tackle next:

* Controller specs
* Cover edge cases like submitting empty fields
* Much better design (this was hacked together way too quickly)
* Prompt before carrying out deletion (one click, and your work is gone!)
* Deploy (probably on Heroku)
* Add a code editor instead of a plain textarea
* Add a marketing-oriented homepage explaining that this is a "diary" for saving interesting code snippets


Sorry, one more edit after the two-hour mark: I would also want to add pagination, tagging, etc.