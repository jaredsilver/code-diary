class ApplicationController < ActionController::Base
  def index
    return redirect_to entries_path if user_signed_in?
    redirect_to new_session_path(User)
  end
end
