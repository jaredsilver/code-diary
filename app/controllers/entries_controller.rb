class EntriesController < ApplicationController
  before_action :authenticate_user!

  def index
    entries = current_user.entries
    return redirect_to new_entry_path unless entries.any?
    render locals: { entries: current_user.entries }
  end

  def show
    entry = Entry.find(params[:id])
    render locals: { entry: entry }
  end

  def new
  end

  def create
    entry = Entry.new(entry_params)
    return redirect_to entry_path(entry) if entry.save!
    flash[:error] = "Something went wrong"
    redirect_to :back
  end

  def edit
    entry = Entry.find(params[:id])
    render locals: { entry: entry }
  end

  def update
    entry = Entry.find(params[:id])
    entry.update!(entry_params)
    flash[:success] = 'Diary entry updated!'
    redirect_to entry_path(entry)
  end

  def destroy
    entry = Entry.find(params[:id])
    entry.destroy!
    flash[:success] = 'Diary entry deleted!'
    redirect_to entries_path
  end

  private

  def entry_params
    params.require(:entry).permit(:title, :body).merge(user_id: current_user.id)
  end
end
